// import Container from 'react-bootstrap/Container';
// import Nav from 'react-bootstrap/Nav';
// import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

import {useState, useContext} from 'react'

import {NavLink} from "react-router-dom";
//shorthand method
import {Container, Nav, Navbar, NavDropdown} from "react-bootstrap";
import UserContext from "../UserContext.js";

export default function AppNavbar() {
  /*
  -"as" prop allows component to be treated as the component of the "react-router-dom" to gain access to it's properties and functionalities.
  */

    const { user } = useContext(UserContext)
    console.log(user);

  return (<Navbar bg="light" expand="lg">
    <Container fluid="fluid">
      <Navbar.Brand as={NavLink} to="/">Zuitt</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav"/>
      <Navbar.Collapse id="basic-navbar-nav">
        {/*className is use instead of class to specify a css class
          ml -> ms (margin start)
          mr -> me (margin end)
          If user is log in, nav links visible:
            -Home
            -Courses
            -Logout
          If user is not login, navlinks visible:
            -Home
            -courses
            -login
            -Register
          */
        }
        <Nav className="ms-auto">
          <Nav.Link as={NavLink} to="/" end>Home</Nav.Link>
          <Nav.Link as={NavLink} to="/courses" end>Courses</Nav.Link>
          {
            (user.id !== null)
              ?
              <Nav.Link as={NavLink} to="/logout" end>logout</Nav.Link>
              :
              <>
              <Nav.Link as={NavLink} to="/login" end>Login</Nav.Link>
              <Nav.Link as={NavLink} to="/register" end>Register</Nav.Link>
              </>
        }
        </Nav>
      </Navbar.Collapse>
    </Container>
  </Navbar>)
}
