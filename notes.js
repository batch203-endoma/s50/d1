//[Section] Creating React Application:
//Syntax:
npx create - react - app < project - name >

  // Delete un necessary files from the app :
  // application > src
  app.test.js
index.css
logo.svg
reportWebVitals.js


/*
  ReactJS Component
    -This are reusable parts of our react application.
    -They are independent UI parts of our app.
    -Components are functions that reeturn react elements.
    -Components naming Convention : PascalCase.
      -Capitalized letter for all words of the function name AND file name associated with.

*/

// React bootstrap Components
/*
  Syntax:
    import {moduleNames} from "file path"


*/

/*
	React.StrictMode is a built-in react component which is used to highlight potential problems in our code and in fact allows for more information about errors in our code.
*/


/*
  React import pattern
    -Imports from built-in react modules.
    -imports from downloaded packages
    -imports from user defined components
*/


/*
  Props
    - is a shorthand for "property" since componets are considered as object in ReactJS
    -is a way to pass data from a parent component to a child component
    - It is synonymous to function parameters.
    - It is used like an HTML attribute added


*/

/*
  States
  -statres are a way to store information within a component. this information cathen be updated within the component.
  -states are used to keep track of information related to individual components.

  hooks
  -Special / react-defind-methods and function that allow us to do certain task in our components.
  -Use the state hook for this component to be able to store its state.

  // useEffect() allows us to perform a "side effects" in your components or run a specific task.
    // some examples of side effects are: fetching data, directly updating the dom, and timers.

    // syntax:
      // useEffect(function,[dependency])

    // useEffect() always runs the task on the initial render and or every render (when a state changes in a component).
      // Initial render is when the component is run or displayed for the first time.


      // Routing and Conditional rendering
        // Using "Browser router", it will allow us to simulated changin pages in react, because by default, react is used for SPA (Single Page Application)

        // buttons -> <link> to navigate to other pages
        // Navbar -> <NavLink> to navigate to other pages and set the active navlink

        //Conditional rendering
          //it Allows us to show a componenets




  // React Context
    // Allows us to pass down and use(consume) data in any component we need in our React application without using "props".
    // In other words, react context allow us to share data ( state, function, etc.) accross component more easily.

        //3 simple steps in using react Context
          //1. creating the context.
          //2. providing the context.
          //3. consuming the context.
          

*/





















//
