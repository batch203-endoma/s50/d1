import {useState, useEffect} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom'
export default function CourseCard({courseProp}) {
  // console.log(props.courseProp.name)
  // console.log(typeof props);
  // console.log(courseProp);

  // Sceneario: keep track the number of enrollees of each course.

  const { _id, name, description, price, slots} = courseProp;

  //Syntax:
  //const [stateName, setStateName] = useState(initialStateValue);
  // Using the state hook, it returns an array with the following elements:
  // first element contains the current initial state value.
  //second element is a function that is used to change the value of the first element.

  // const [count, setCount] = useState(0);
  // const [seats, setSeats] = useState(30);
  //use the disabled state to disable the enroll button.
  // const [disabled, setDisabled] = useState(false);
  // console.log(useState(10))

  // Syntax:
  // useEeffect(function, [dependencyArray])

  // No dependency passed
  // If the useEffect() does not have a dependecy array, it will run on initial render and whenever a state is set by its function.
  // useEffect(() => {
  //   console.log("Will run on initial render or on every changes with our components");
  // });

  // An empty array
  // if the useEffect() has dependecy array but it is empty, it will only run on the initial render.
  // useEffect(()=>{
  //   console.log("Will only run on initial render.");
  // }, [])

  // with dependency array (props or state values);
  // useEffect(() => {
  //   console.log("will run on initial render and every change on the dependcy value.");
  // }, [seats, count])

  // useEffect(()=>{
  //   if( seats <= 0){
  //     setDisabled(true);
  //     alert("No more seats Available")
  //   }
  // },[seats])

  // function unEnroll() {
  //   setCount(count - 1);
  // }

  // Function that keeps track of the enrollees for a course.
  //
  /*
    We will refactor the "enroll" function using the "useEffect" hooks to disable the enroll button the seats reach zero
  */
  // function enroll() {
  //   //  Activity Solution
  //   //   if (count < 30) {
  //   //     setCount(count + 1);
  //   //     setSeats(seats - 1)
  //   //
  //   //   }
  //   //   if (count == 30) {
  //   //     alert("No more seat available")
  //   //     console.log(`Enrollees: ${count}`)
  //   //     console.log(`Seats: ${seats}`)
  //   //   }
  //
  //   setCount(count + 1);
  //   console.log(`Enrollees:${count}`);
  //   setSeats(seats - 1);
  //   console.log(`seats:${seats}`);
  //
  // }
  return (<Row>
    <Card className="my-3">
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
        <Card.Text>
          {description}
        </Card.Text>
        <Card.Subtitle className="mb-2 text-muted">price:</Card.Subtitle>
        <Card.Text>
        Php  {price}
        </Card.Text>
        <Card.Subtitle>
          Slots:
        </Card.Subtitle>
        <Card.Text>
          {slots}
          available
        </Card.Text>
        <Button as = {Link} to={`/courses/${_id}`}variant="primary" >details</Button>
      </Card.Body>
    </Card>
  </Row>)
}
