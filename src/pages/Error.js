import Banner from '../components/Banner.js';

export default function Error(){
  const data = {
    title: "404 - Not found",
    description: "The page you are looking for cannot be found",
    destination:"/",
    label:"Back to home"
  }
  return(
    <Banner data={data} />
  )
}
