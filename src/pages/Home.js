import Banner from "../components/Banner.js";
import Highlights from "../components/Highlights.js"

export default function Home() {
  const data = {
    title: "Zuitt Coding Bootcamp",
    description: "Oppurtunities for everyone, everywhere",
    destination:"/Courses",
    label:"Enroll Now"
  }
  return (
    <>
      <Banner data={data} />
      <Highlights />
    </>

  )
}
