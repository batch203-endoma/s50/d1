import CourseCard from "../components/CourseCard.js"
// import coursesData from "../data/CoursesData.js";
import {useEffect, useState} from "react";
export default function Courses() {

  const [courses, setCourses] = useState([]);
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/`).then(res => res.json()).then(data => {
      console.log(data);
      setCourses(data.map(course => {
        return (<CourseCard key={course._id} courseProp={course}/>)
      }))
    })
  }, []);
  // console.log(coursesData)
  // console.log(coursesData[0]);
  //The curly braces({}) are used for props to signify that we are providing information using JavaScript expressions.

  //everytime the map method loops through the data, it creates "CourseCard" component and passes the current element in our courseData array using course prop.
  // const courses = coursesData.map(course => {
  //   return(
  //     <CourseCard key={course.id} courseProp={course}/>
  //   )
  // })
  return (
    <>
     <h1> Courses</h1>
     {courses}


     < />

  )
}
