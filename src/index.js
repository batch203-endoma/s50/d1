import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

//Import the bootstrap 5 css
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);



/*

*/


/*
   root.render() - allows us to render/display our reactjs elements and show it in our HTML documents.
   root.render(<reactelemetn>)
*/

// ReactDOM
// const name = "John smith";
//
// // const element = <h1>Hello, {name}</h1>
// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }
//
// function formatName(fullName) {
//   return `${fullName.firstName} ${fullName.lastName}`
// }

// the "h1" tag is an example of what we call JSX
  // It allows us to create HTML elements and as the same time allows us to apply JavaScript code to our elements.
  //JSX makes it easier to write both HTML and javascript  code in a single file as opposed for to creating separate files
// const element = <h1> Hello, {formatName(user)}</h1>
//
// root.render(element);
