import {useState, useEffect} from "react"
import {BrowserRouter as Router, Routes, Route} from "react-router-dom"
import {Container} from "react-bootstrap"
import AppNavbar from "./components/AppNavbar.js";
import Home from "./pages/Home.js"
import Courses from "./pages/Courses.js"
import Register from "./pages/Register.js"
import Login from "./pages/Login.js"
import './App.css';
import Logout from "./pages/Logout.js"
import Error from "./pages/Error.js"
import {UserProvider} from "./UserContext";
import CourseView from "./pages/CourseView.js";

/*
  All other components/pages will be contained in our main(parent) component: <app />

  we use react fragment to render adjacent elements
*/

//Create a "user" state and an unsetUser" function that will be used in different pages/componenets within the application

// Global state hooks for the user information for validating if a user is logged in

function App() {

  const [user, setUser] = useState({id: null, isAdmin: null})

  //function for clearing localstorage

  const unsetUser = () => {
    localStorage.clear()
  }
  // console.log(user)

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  // to update the User state upon a page load is initiated and if a user already exist.

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then(res => res.json()).then(data => {
      console.log(data);
      if (data._id !== undefined) {
        setUser({id: data._id, isAdmin: data.isAdmin})
      } else {
        setUser({id: null, isAdmin: null});
      }
    })
    // Token will be sent as part of the request's header.

  }, [])
  return (
  //We store information in the context by providing the information using the "UserProvider" component and passing the information via the "value" prop.
  // all the information inside the table
  <UserProvider value={{
      user,
      setUser,
      unsetUser
    }}>
    {/* Router component is used to wrapped around all componenets which will have access to the routing system
        */
    }
    <Router>
      <AppNavbar/>
      <Container>
        {/* Routes holds all our Route components. */}
        <Routes>
          {/*
          Route asssign an endpoint and display the appropriate page component for that endpoint.
          - exact and patch props to assign the endpoint and page should be only accessed on the specific endpoint
          -"element" props assign page components to the displayed enpoint.*/
          }
          <Route exact="exact" path="/" element={<Home />}/>
          <Route exact="exact" path="/courses" element={<Courses />}/>
          <Route exact="exact" path="/courses/:courseId" element={<CourseView />}/>
          <Route exact="exact" path="/login" element={<Login />}/>
          <Route exact="exact" path="/register" element={<Register />}/>
          <Route exact="exact" path="/logout" element={<Logout />}/>
          <Route path="*" element={<Error />}/>
        </Routes>
      </Container>
    </Router>;
  </UserProvider>)
}
export default App;

/*
    Mini Activity

        1. Create a "CourseCard" component showing a particular course with the name, description and price inside a React-Bootstrap Card.
            - Import the "Card" and "Button" in the react-bootstrap.
            - The "Card.Body" should contain the following:
                - The course name should be in the "Card.Title".
                - The "description" and "price" label should be in the "Card.Subtitle".
                - The value for "description" and "price" should be in the "Card.Text".
                - Add a "Button" with "primary" color for the Enroll.
        2. Create a "Courses.js" page and render the "CourseCard" component inside of it.
        3. Render also the "Courses" page in the parent component to mount/display it in our browser.
        4. Take a screenshot of your browser and send it in the batch hangouts

    */
