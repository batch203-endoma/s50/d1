import { useState, useEffect, useContext } from "react"
import { Navigate } from "react-router-dom"
import { Form, Button } from "react-bootstrap";
import UserContext from "../UserContext.js"
import Swal from "sweetalert2";



/*s52 Activity:
1. Create a Login page that simulates user login and authentication using an email and a password.
2. When all the input fields are filled, enable the submit button.
3. Upon clicking the submit button, a message will alert the user of a successful login.
4. Update your remote repository for react-booking app.
5. Add another the remote link and push to git with the commit message of Add activity code - S52.
6. Add the link in Boodle.*/

//Allow us to consume the UserContext object and its values(properties to use for user validation.)



export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);
  const { user, setUser } = useContext(UserContext)

  // const navigate =useNavigate();

  useEffect(() => {

    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
    //enable the button if:
    // all the fields are populated
    // both passwords match
  }, [email, password])

  function loginUser(e) {
    e.preventDefault();
    //clear input fields
    // prevents page loading/redirection via form submission


    /*
      Syntax:
      fetch ("url", {options})
      .then(res => res.json())
      .then(data => {})

    */

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data.accessToken);
        if (data.accessToken !== undefined) {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);
          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Zuitt"
          });
        }
        else {
          Swal.fire({
            title: "authentication failed",
            icon: "error",
            text: "Please check your password"
          });
        }
      })


    // "localStorage" is a property that allows JavaScript sites and application to save key-value pairs in a web browser with no expiration date.

    // Syntax:
    //LocalStorage.setItem ("propertyName", value)

    // localStorage.setItem("email", email);
    //
    // setUser({
    //   email: localStorage.getItem("email")
    // })

    setEmail('');
    setPassword('');
    // alert(`${email} has been verified`)
    //redirect us to home
    // navigate("/");
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        // Change the global user state to store the "id" and isAdmin
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      })
    // Token will be sent as part of the request's header.


  }
  return (
    (user.id !== null)
      ?
      <Navigate to="/courses" />
      :
      <>
        <h1 className="my-5 text-center">Login </h1>
        <Form onSubmit={e => loginUser(e)}>
          <Form.Group className="mb-3" controlId="email">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter Address"
              value={email}
              required
              onChange={e => setEmail(e.target.value)} />
          </Form.Group>
          <Form.Group className="mb-3" controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={e => setPassword(e.target.value)} />
          </Form.Group>
          {
            isActive
              ?
              <Button variant="success" type="submit" id="submitBtn">
                Submit
            </Button>
              :
              <Button variant="success" type="submit" id="submitBtn" disabled>
                Submit
            </Button>
          }
        </Form>
      </>
  )
}



















//
