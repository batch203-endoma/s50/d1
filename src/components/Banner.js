import { Link } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";

export default function Banner({data}) {

  const {title, description, destination, label } = data
  return (
    <Row>
      <Col className="p-5 text-center">
        <h1> {title} </h1>
        <p>{description}</p>
        <Button as = {Link} to ={destination} variant="primary">{label}</Button>
      </Col>
    </Row>
  )
}
/*
s53 Activity Instructions:
1. Create a route which will be accessed when a user enters an undefined route and display the Error page.
2. Refactor the Banner component to be reusable for the Error page and the Home page.
a. Create an object variable named data that will contain the following properties:
- title
- description
-  destination
- label
b. Use the data variable as prop for the Banner that will be passed from the parent component (Home and Error page).
3. Update the local repository, and push to git with the commit message of Add activity code - S53.
4. Add the link in Boodle.*/
